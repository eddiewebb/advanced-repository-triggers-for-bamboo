#Advanced Repositroy Triggers for Bamboo

## Features
Alternative to https://confluence.atlassian.com/display/BAMBOO/Repository+triggers+the+build+when+changes+are+committed

Currently supports Subversion repositories for large installations where one SVN repository may server many projects, and therefore many plans.  **Also does not require matching IPs**

A single standard post-commit hook script will work across all repos, letting bamboo determine which plans to build if they use that project repository AND have changed since last build.



## Linked Repository Triggering
Linked Repositories are the suggested standard for all plans.

- A single post-commit will work across all SVN repos, regardless of hoe the projects and plans evolve overtime.
- The path must match that URL in bamboo, including protocol (file://, https:// etc.)
- Only plans with revision changes will actually build.
- Any plan without "repository triggers the build when changes are committed" will be ignored.
- IP Settings for the trigger are ignored, an upcomming enhancement will include central auth for remote IPs.

### Example

**Plan AAA-ONE**

  Repo: "Team One, Project One" - https://server.com/repos/team1/project1
  Trigger: "Repository triggers on changes" (empty IP space)

**Plan AAA-TWO**

  Repo: "Team One, Project Two" - https://server.com/repos/team1/project2
  Trigger: "Repository triggers on changes" (empty IP space)


**Plan BBB-ONE**

  Repo: "Team Two, Single Project" - https://server.com/repos/team2
  Trigger: "Repository triggers on changes" (empty IP space)


#### Team One example subversion post-commit config  (/repo/hooks/post-commit)

``` bash
#!/bin/bash

repo=$1 # remember this is the FS path as seen by the SVN tool, NOT the FQDN seen by the user. You may need to append to the proper host/protocol

curl -X POST "http://localhost:6990/bamboo/rest/linkedrepositorytrigger/1.0/commit" -d '{"path":"'$repo'"}' -H "Content-type: text/json"
```

#### Team One Project one files change
Result: Plugin asks Plan AAA-ONE & AAA-TWO to poll the repository.  Only AAA-ONE builds seeing a change.

#### Team One Project TWO files change
Result: Plugin asks Plan AAA-ONE & AAA-TWO to poll the repository.  Only AAA-TWO builds seeing a change.



## USing the Atlassian SDK 

Here are the SDK commands you'll use immediately:

* atlas-run   -- installs this plugin into the product and starts it on localhost
* atlas-debug -- same as atlas-run, but allows a debugger to attach at port 5005
* atlas-cli   -- after atlas-run or atlas-debug, opens a Maven command line window:
                 - 'pi' reinstalls the plugin into the running product instance
* atlas-help  -- prints description for all commands in the SDK

Full documentation is always available at:

https://developer.atlassian.com/display/DOCS/Introduction+to+the+Atlassian+Plugin+SDK