package it.com.edwardawebb.bamboo.triggers.rest;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response.StatusType;

import org.junit.Test;
import org.junit.After;
import org.junit.Before;
import org.mockito.Mockito;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import com.edwardawebb.bamboo.triggers.rest.LinkedRepositoryTriggerResource;
import com.edwardawebb.bamboo.triggers.rest.LinkedRepositoryTriggerResourceModel;

import org.apache.wink.client.ClientResponse;
import org.apache.wink.client.Resource;
import org.apache.wink.client.RestClient;

public class LinkedRepositoryTriggerResourceFuncTest {

    @Before
    public void setup() {

    }

    @After
    public void tearDown() {

    }

    @Test
    public void messageIsValid() {

        String baseUrl = System.getProperty("baseurl");
        String resourceUrl = baseUrl + "/rest/linkedrepositorytrigger/1.0/commit";

        RestClient client = new RestClient();
        Resource resource = client.resource(resourceUrl);

        ClientResponse response = resource.contentType(MediaType.APPLICATION_JSON_TYPE).post("{\"path\":\"/foo\"}");
        
        assertEquals("unexpected exception, non-passing request",200,response.getStatusCode());
    }
}
