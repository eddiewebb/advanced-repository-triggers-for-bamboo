package ut.com.edwardawebb.bamboo.triggers.rest;

import org.junit.Test;
import org.junit.After;
import org.junit.Before;
import org.mockito.Mockito;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import com.atlassian.bamboo.repository.RepositoryDefinitionManager;
import com.edwardawebb.bamboo.triggers.rest.LinkedRepositoryTriggerResource;
import com.edwardawebb.bamboo.triggers.rest.LinkedRepositoryTriggerResourceModel;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.GenericEntity;

public class LinkedRepositoryTriggerResourceTest {

    @Before
    public void setup() {

    }

    @After
    public void tearDown() {

    }

    @Test
    public void messageIsValid() {
        LinkedRepositoryTriggerResource resource = new LinkedRepositoryTriggerResource();
        resource.setRepositoryDefinitionManager(mock(RepositoryDefinitionManager.class));

        Response response = resource.getMessage(new LinkedRepositoryTriggerResourceModel("foo"));
        final String message = (String) response.getEntity();

        assertEquals("wrong message","Success",message);
    }
}
