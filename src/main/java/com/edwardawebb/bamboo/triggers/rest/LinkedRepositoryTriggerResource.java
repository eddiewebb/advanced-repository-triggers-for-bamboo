package com.edwardawebb.bamboo.triggers.rest;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;

import com.atlassian.bamboo.build.strategy.BuildStrategy;
import com.atlassian.bamboo.build.strategy.TriggeredBuildStrategy;
import com.atlassian.bamboo.plan.Plan;
import com.atlassian.bamboo.plan.PlanIdentifier;
import com.atlassian.bamboo.plan.PlanKey;
import com.atlassian.bamboo.plan.PlanManager;
import com.atlassian.bamboo.plan.branch.ChainBranchIdentifier;
import com.atlassian.bamboo.plan.cache.ImmutableChainBranch;
import com.atlassian.bamboo.repository.RepositoryData;
import com.atlassian.bamboo.repository.RepositoryDefinitionManager;
import com.atlassian.bamboo.util.CacheAwareness;
import com.atlassian.bamboo.v2.events.ChangeDetectionRequiredEvent;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;

/**
 * A resource of message.
 */
@Path("/commit")
public class LinkedRepositoryTriggerResource {
    private static final Logger log = Logger.getLogger(LinkedRepositoryTriggerResource.class);
    private static final String REPO_PLUGIN_KEY_SVN = "com.atlassian.bamboo.plugin.system.repository:svn";
    private RepositoryDefinitionManager repositoryDefinitionManager;
    private PlanManager planManager;
    private EventPublisher eventPublisher;
    
    @POST
    @AnonymousAllowed
    @Produces({MediaType.APPLICATION_JSON})
    public Response getMessage(LinkedRepositoryTriggerResourceModel request)
    { 
        List<Long> repositoryIds = findRepositoryIdFromSvnCommitPath(request.getPath());
        List<PlanIdentifier> plans = findAllPlansWithLinkedRepository(repositoryIds);
        log.debug("Triggering plans: " + plans);
        for (PlanIdentifier planIdentifier : plans) {
            if(!planIdentifier.isSuspendedFromBuilding()){
               Plan plan = planManager.getPlanByKey(planIdentifier.getPlanKey());
               if ( null != plan ){
                   TriggeredBuildStrategy triggeredBuildStrategy = null;
                   for (BuildStrategy buildStrategy : plan.getBuildDefinition().getBuildStrategies()){
                           if (buildStrategy instanceof TriggeredBuildStrategy)
                           {
                                triggeredBuildStrategy = (TriggeredBuildStrategy)buildStrategy;
                                break;
                           }
                   }

                   if (null == triggeredBuildStrategy)
                   {                       
                          log.debug("The plan \"" + plan.getName() + "\" is not configured for remote triggering.");
                   }else{
                          log.info("Requesting build for plan \"" + plan.getName() + "\" from Remote Aggregate Trigger");
                       eventPublisher.publish(createChangeDetectionEvent(triggeredBuildStrategy, plan.getPlanKey()));
                   }
                      

                   //TODO - does not support branches yet, see TriggerRemoteBuild:65
                   
                   
               }
            }
        }
        return Response.ok("Success").build();
    }
    
    
    private List<PlanIdentifier> findAllPlansWithLinkedRepository(List<Long> repositoryIds){
        List<PlanIdentifier> allPlans = new ArrayList<PlanIdentifier>();
        for (long       repositoryId    : repositoryIds) {
            allPlans.addAll(repositoryDefinitionManager.getIdentifiersOfPlansUsingRepository(repositoryId));
        }
        
        return allPlans;
    }
    
    /*
     * This uses the commit repo path (argumenr #1 of an SVN commit hook)
     * IMPORTANT!! This is the top level repo path excluding server
     * '/svn/project-name'
     * '/repos/team1'
     * NOT the path commited against. 
     * 
     * For that reason MANY repositories may match if repos contain many projects
     * '/repos/team1/project1','/repos/team1/project2'
     */
    private List<Long> findRepositoryIdFromSvnCommitPath(String path){
        List<Long> mathingPlanIds = new ArrayList<Long>();
        List<? extends RepositoryData> repositoryDefinitions = getRepositoryDefinitions();
        log.debug("Checking for global repos");
        for (RepositoryData repoData : repositoryDefinitions) {
            log.debug(repoData);
            if(repoData.getPluginKey().equals(REPO_PLUGIN_KEY_SVN)){
                log.debug("repo '" + repoData.getName() + "' is SVN, checking paths");
                //log.debug( ReflectionToStringBuilder.toString(repoData.getRepository()));
                String url = repoData.getRepository().getLocationIdentifier();
                log.debug(url);
                if(url.contains(path)){
                    log.info("Plans dependent on Repository " + repoData.getId() +":" + repoData.getName() + " will be trigger by a commit to path " + path);
                    mathingPlanIds.add(repoData.getId());
                }
            }
        }
        return mathingPlanIds;
    }
    
    private List<? extends RepositoryData> getRepositoryDefinitions()
    {       
        log.debug("popualting  list");
        List<? extends RepositoryData> repositoryDefinitions = repositoryDefinitionManager.getGlobalRepositoryDefinitionsUnrestricted();
        log.debug("done, " + repositoryDefinitions.size() + " repos found");
        
        return repositoryDefinitions;
    }
    private ChangeDetectionRequiredEvent createChangeDetectionEvent(final TriggeredBuildStrategy triggeredBuildStrategy, final PlanKey planKey)
    {
        final ChangeDetectionRequiredEvent changeDetectionRequiredEvent = new ChangeDetectionRequiredEvent(this, planKey.toString(), triggeredBuildStrategy.getId(), triggeredBuildStrategy.getTriggeringRepositories(), triggeredBuildStrategy.getTriggerConditionsConfiguration(), true);
        changeDetectionRequiredEvent.setCachesToIgnore(CacheAwareness.CHANGE_DETECTION);
        return changeDetectionRequiredEvent;
    }
    
    
    
    
    
    
    
    
    
    
    
    

    
    public void setRepositoryDefinitionManager(final RepositoryDefinitionManager repositoryDefinitionManager)
    {
        this.repositoryDefinitionManager = repositoryDefinitionManager;
    }
    

    
    public void setPlanManager(final PlanManager planManager)
    {
        this.planManager = planManager;
    }
    public void setEventPublisher(EventPublisher eventPublisher)
    {
        this.eventPublisher = eventPublisher;
    }
}