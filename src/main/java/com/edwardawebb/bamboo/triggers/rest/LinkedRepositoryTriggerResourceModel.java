package com.edwardawebb.bamboo.triggers.rest;

import javax.xml.bind.annotation.*;
@XmlRootElement(name = "commit")
@XmlAccessorType(XmlAccessType.FIELD)
public class LinkedRepositoryTriggerResourceModel {

    @XmlElement(name = "path")
    private String path;

    public LinkedRepositoryTriggerResourceModel() {
    }

    public LinkedRepositoryTriggerResourceModel(String path) {
        this.path = path;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}